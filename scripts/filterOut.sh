#!/bin/sh

#Usage filterOutDNAC.sh ftout_input($1) output($2) sample($3) 

#get param
INPUT_FILE=$1
OUTPUT_FILE=$2
NAME=$3


##withambigous
awk -v nam=$NAME ' BEGIN {
	print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tCopyNum_N/T\tOnco_State"
} NR > 1 {
	if ($1 != "Y") {
		if ($5 == 1) {
			typ="DEL"
		} else {
			typ="DUP"
		}
		print "SCoNEs_2.1\t" nam "\t" $1 "\t" $2*1 "\t" $3*1 "\t" $1 "\t" $3-$2 "\t" typ "\t" $4 "\t" $6
	}
} ' $INPUT_FILE > $OUTPUT_FILE

